import time
from datetime import datetime, timedelta
import datetime as dt
from time import mktime
import pickle
from pandas.io.parsers import read_csv
import pandas as pd
import datetime as dt
import numpy as np
import os
from decimal import Decimal



def convert_csv_to_df_pickle_finam(filename):
    df = read_csv(filename, names=['date', 'time', 'price', 'volume'],
                  parse_dates={'datetime':['date','time']},
                  keep_date_col = True,
                  low_memory=False,
                  index_col='datetime')
    del df['time']
    del df['date']
    #print(df.index)
    print('Read!')
    pickle.dump(df, open('panda_frame_ticks_' + filename + '.p', 'wb'))
    print('Dumped')

def take_file_name(filename):
    return filename[filename.rfind('/')+1:filename.rfind('.')]

def is_file_already_there(filename, s=''):
    if os.path.exists(filename):
        print(filename, 'is already there! exit', s)
        return True
    return False

DATE_FORMAT = '%Y-%m-%d'
def convert_csv_to_df_pickle_qscalp(root_dir, stock_name, date):
    filename = root_dir + 'csv/{0}/Ticks.{0}.{1}.csv'.format(stock_name,
                                                     date.strftime(DATE_FORMAT))
    output_filename = root_dir +\
        'df_ticks/{0}/panda_frame_ticks_{0}.{1}.p'.format(stock_name, date.strftime(DATE_FORMAT))
    if is_file_already_there(output_filename, 'convertion'):
        return output_filename
    if not os.path.exists(filename):
        return None
    #dateparser = lambda x: pandas.to_datetime(x, '%Y-%m-%d %H:%M:%S')
    df = read_csv(filename, #infers automatically names=['date', 'price', 'dir', 'volume'],
                  parse_dates=['date'],
                  dtype={'price' : str},
                  infer_datetime_format=True,
                  keep_date_col=True,
                  low_memory=False,
                  index_col='date')
    #df.set_index('date', inplace=True)
    prematket_date = df.index.values[0]
    df = df[df.index.values > prematket_date]
    #df['price'] = df['price'].map(Decimal)
    df['price'] = df['price'].astype('float64')
    #self.rel_price.quantize(self.price_step,
    #context=Context(traps=[Inexact])),

    print('Read and converted!', filename)
    pickle.dump(df, open(output_filename, 'wb'))
    print('Dumped', output_filename)
    return output_filename

def get_interval_in_sec(interval):
    if interval == 'ticks':
        return 0
    if 'min' in interval:
        return int(interval[:-3])*60
    if 's' in interval:
        return int(interval[:-1])
    if 'h' in interval:
        return int(interval[:-1])*3600
    if 'D' in interval:
        return int(interval[:-1])*3600*24
    return 99999999

def convert_df_ticks_into_intervals(root_dir, stock_name, date, interval='1min'):
    filename = convert_csv_to_df_pickle_qscalp(root_dir, stock_name, date)
    if filename is None:
        return None
    if interval == 'ticks':
        return filename
    output_filename = root_dir +\
        'df_{2}/{0}/panda_frame_{2}_{0}.{1}.p'.format(stock_name,
                date.strftime(DATE_FORMAT), interval)
    s = root_dir + 'df_' + interval

    if not os.path.exists(s):
        os.makedirs(s)
        create_stock_folders(s + '/')

    if is_file_already_there(output_filename, 'to Minutes'):
        return output_filename

    df = load_df(filename)
    df = resample_raw_df(df, interval)
    save_df(output_filename, df)
    return output_filename

def resample_raw_df(df, timeframe):
    del df['dir']
    #df['price'] = df['price'].astype('float64')
    #print(type(df['price'][0]))
    gr_by_price = df['price'].resample(timeframe, how='ohlc')

    gr_by_volume = df['volume'].resample(timeframe, how='sum')
    df = gr_by_price.join(gr_by_volume)
    for i, vl in enumerate(df['volume']):
        if np.isnan(vl):
            df['volume'][i] = 0
            df['open'][i] = df['close'][i-1]
            df['close'][i] = df['close'][i-1]
            df['high'][i] = df['close'][i-1]
            df['low'][i] = df['close'][i-1]
    print('Agregated!')
    return df

def save_df(filename, df):
    pickle.dump(df, open(filename, 'wb'))
    print('Dumped into', filename)

def load_df(filename):
    if filename is None:
        return None
    print('Loaded', filename)
    return pd.read_pickle(filename)

def create_stock_folders(path):
    stock_list = ['GAZP', 'GMKN', 'HYDR', 'LKOH', 'MGNT', 'MTSS', 'NVTK', 'ROSN',
                  'RTKM', 'SBER', 'SNGS', 'TATN', 'TRNFP', 'URKA', 'VTBR']
    for stock in stock_list:
        directory = path + stock
        if not os.path.exists(directory):
            os.makedirs(directory)

def get_day_df(root_dir, stock_name, date, interval='ticks'):
    return load_df(convert_df_ticks_into_intervals(root_dir, stock_name,
                   date, interval))


def get_data_between_dates(root_dir, stock_name, start_date, end_date,
                           interval='ticks', source=None):
    delta = timedelta(days=1)
    d = start_date
    data = []
    while d <= end_date:
        data_c = None
        if source is not None and pd.to_datetime(d).date() in source[stock_name][interval]:
            data_c = source[stock_name][interval]
        else:
            data_c = get_day_df(root_dir, stock_name, d, interval)
        if data_c is None:
            d += delta
            continue

        data.append(data_c)
        d += delta
    return data

def get_df_between_dates(root_dir, stock_name, start_date, end_date,
                         interval='ticks', source=None):
    data = get_data_between_dates(root_dir, stock_name, start_date, end_date,
                                interval, source)
    all_in_df = None
    if len(data) > 0:
        all_in_df = data[0]
    for i in range(1, len(data)):
        all_in_df = all_in_df.append(data[i])
    return all_in_df, data


