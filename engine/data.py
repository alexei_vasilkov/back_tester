import datetime as dt
from datetime import timedelta
import os, os.path
import pandas as pd

from abc import ABCMeta, abstractmethod

from back_tester.engine.event import MarketEvent

import pickle
import numpy as np
from back_tester.engine.df_converter import get_df_between_dates
from collections import defaultdict

class DataHandler(object):
    """
    DataHandler is an abstract base class providing an interface for
    all subsequent (inherited) data handlers (both live and historic).

    The goal of a (derived) DataHandler object is to output a generated
    set of bars (OLHCVI) for each symbol requested.

    FIXME TODO replace with ohlc

    This will replicate how a live strategy would function as current
    market data would be sent "down the pipe". Thus a historic and live
    system will be treated identically by the rest of the backtesting suite.
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def get_last_n_units(self, symbol, interval='ticks', N=1):
        raise NotImplementedError("Should implement get_last_n_units()")

    @abstractmethod
    def new_data_arrived(self):
        raise NotImplementedError("Should implement new_data_arrived()")

    @abstractmethod
    def get_data_units_between_dates(self, symbol, start_date, end_date,
                                     interval='ticks'):
        raise NotImplementedError("Should implement get_data_units_between_dates()")




class DataStorage():

    def __init__(self, max_size=150000):
        self.max_size = max_size
        self.dfs = {}

    def append_new_data(self, symbol, new_data_df, interval='ticks'):
        data_len = len(new_data_df.index)
        data_key = symbol + interval
        if data_len > self.max_size:
            print('New data df is too large! datastorage message', data_len)
            return False

        if data_key not in self.dfs:
            self.dfs[data_key] = new_data_df
            return True

        new_df = self.dfs[data_key].reindex(self.dfs[data_key].index.union(new_data_df.index))
        new_df.update(new_data_df)
        self.dfs[data_key] = new_df
        if data_len > self.max_size:
            new_len = data_len - self.max_size
            s = new_data_df.index.values[0]
            e = new_data_df.index.values[-1]
            print(s, e)
            s1 = self.dfs[data_key].index.values[0]
            e1 = self.dfs[data_key].index.values[-1]
            #remove far end
            if s - s1 < e1 - e:
                self.dfs[data_key] = self.dfs[data_key][:new_len]
            else:
                self.dfs[data_key] = self.dfs[data_key][len(self.dfs[data_key].index) - new_len:]

        return True



    def get_all_stored_data(self, symbol, interval='ticks'):
        data_key = symbol + interval
        if data_key not in self.dfs:
            print(data_key, 'is not in the storage yet')
            return None
        return self.dfs[data_key]

    def get_data_between_dates(self, symbol, interval='ticks'):

        return None



class HistoricDFDataHandler(DataHandler):

    def __init__(self, events, data_path, symbol_list, last_point,
                 window_days_size=3, storage_depth=5, interval='5min'):
        self.events = events
        self.data_path = data_path
        self.symbol_list = symbol_list
        self.last_point = last_point
        #finde nearest valid point
        self.window_days_size = timedelta(days=window_days_size)
        self.loaded_symbol_data = defaultdict(lambda: defaultdict(dict))
        self.last_asked_symbol_data = defaultdict(dict)
        self.last_point = defaultdict(dict)
        for symbol in symbol_list:
            self.fetch_data_units_between_dates(symbol, last_point -
                                         self.window_days_size, last_point,
                                         interval)
            self.last_point[symbol][interval] =\
             (0,#len(self.last_asked_symbol_data[symbol][interval].index)-1
                pd.to_datetime(self.last_asked_symbol_data[symbol][interval].index.values[0]).date())
        self.new_data_arrived()



    def new_data_arrived(self):
        #self.events.put(MarketEvent())
        print('Market event!')

    def get_last_data(self):
        return self.last_asked_symbol_data

    def set_last_unit(self, symbol, interval, pos):
        poss = pos
        if pos >=\
                len(self.last_asked_symbol_data[symbol][interval].index.values):
            poss = -1
        self.last_point[symbol][interval] =\
             (pos,#len(self.last_asked_symbol_data[symbol][interval].index)-1
                pd.to_datetime(self.last_asked_symbol_data[symbol][interval].index.values[poss]).date())


    def get_next_unit(self, symbol, interval='ticks'):
        q = self.last_point[symbol][interval]
        q = (q[0] + 1, q[1])
        self.last_point[symbol][interval] = q
        if q[0] ==\
            len(self.last_asked_symbol_data[symbol][interval].index.values) and\
            q[1] ==\
                    pd.to_datetime(self.last_asked_symbol_data[symbol][interval].index.values[-1]).date():

                d = timedelta(days=1)
                date = self.last_point[symbol][interval][1] + d
                i = 0
                self.last_point[symbol][interval] = (0, self.fetch_data_units_between_dates(symbol, date,
                                                              date,\
                                                        interval))
                #TODO
                while self.last_point[symbol][interval][1] is None and i < 20:
                    date += d
                    self.last_point[symbol][interval] = (0, self.fetch_data_units_between_dates(symbol, date,
                                                              date,\
                                                        interval))
                    i += 1

                if i == 20:
                    print('No more data for', symbol)
                    return None
                res = self.last_asked_symbol_data[symbol][interval].iloc[0]
                return res
        res =\
            self.last_asked_symbol_data[symbol][interval].iloc[self.last_point[symbol][interval][0]]
        return res

    def update_with_loaded_data(self, symbol, df_array, df, interval):
        self.last_asked_symbol_data[symbol][interval] = df

        if len(self.loaded_symbol_data[symbol][interval]) + len(df_array) > 10:
            del self.loaded_symbol_data[symbol][interval]

        for df in df_array:
            date_key = pd.to_datetime(df.index.values[0]).date()
            self.loaded_symbol_data[symbol][interval][date_key] = df


    def fetch_data_units_between_dates(self, symbol, start_date, end_date,
                                     interval='ticks'):
        df, df_array = get_df_between_dates(self.data_path, symbol, start_date, end_date,
                                            interval, source=self.loaded_symbol_data)
        if df is None:
            return None
        #TODO
        #REMOVE OLD DATA
        #print(start_date, end_date)
        self.update_with_loaded_data(symbol, df_array, df, interval)
        return pd.to_datetime(df_array[-1].index.values[0]).date()

