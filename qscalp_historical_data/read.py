import binascii as ba
import sys
import gzip, zlib
import io
import time
from datetime import timezone
import datetime as dt
import struct
import pytz
from decimal import Decimal, getcontext, Context, Inexact
import csv

MSC_TZ = pytz.timezone('Europe/Moscow')
TICKS_PER_MILLISECOND = 10000
ZERO_DT = dt.datetime(1, 1, 1)

def read_string(f):
    str_len = read_u_leb128(f)
    s = f.read(str_len)

    try:
        s = s.decode('utf-8')
    except Exception:
        pass
    return s

def read_int32(f):
    '''read 4 bytes and convert to an int (from int32)'''
    i=struct.unpack("<i",f.read(4))[0]
    #print "reading int of size '%s'"%i
    return i

def read_int64(f):
    '''read 8 bytes and convert to an int (from int32)'''
    i=struct.unpack("q",f.read(8))[0]
    #print "reading int of size '%s'"%i
    return i

def read_datetime(f):
    ticks = read_int64(f)
    datetime = dt.datetime(1, 1, 1) + dt.timedelta(microseconds = ticks/10)
    return datetime, ticks/TICKS_PER_MILLISECOND

def read_byte(f):
    b = f.read(1)
    if b != None:
        return int.from_bytes(b, byteorder='big', signed=False)
    else:
        return None

def read_leb128(f):
    result = 0
    shift = 0
    size = 64
    while True:
        b = ord(f.read(1))
        result |= (b & 0x7f) << shift
        shift += 7
        if b & 0x80 == 0:
            break
    #/* sign bit of byte is second high order bit (0x40) */
    if (shift < size) and (b & 0x40 != 0):
        result |= - (1 << shift)
    return result


def read_u_leb128(f):
    global EOF
    'decode LEB128 numbers, see http://en.wikipedia.org/wiki/LEB128'
    result = 0
    shift = 0
    try:
        while True:
            b = ord(f.read(1))
            result |= (b & 0x7f) << shift
            if b & 0x80 == 0:
                break
            shift += 7
    except Exception as err:
        EOF = True

    #print "LEB128 buffer: '0x%s'"%buff.encode('hex')
    return result#, size

def read_growing_date_time(f, prev_ticks):
    ticks = read_growing(f, prev_ticks)
    #ticks = ticks*TICKS_PER_MILLISECOND
    #print(ticks)
    #print(ticks/10*TICKS_PER_MILLISECOND)
    datetime = ZERO_DT + dt.timedelta(microseconds =\
              ticks/10*TICKS_PER_MILLISECOND)
    #print(datetime)
    return datetime, ticks




def utc_to_msc(utc_dt):
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=MSC_TZ)

def read_file_header(f):
    signature = 'QScalp History Data'.encode('UTF-8')
    if signature != f[:len(signature)]:
        ba = bytearray(f)
        f = zlib.decompress(bytes(ba), 15+32)
    if signature != f[:len(signature)]:
        print('Wrong signature')
        raise

    f = io.BytesIO(f)

    f.read(len(signature))

    version = read_byte(f)

    program_name = read_string(f)
    comment = read_string(f)
    file_start_date_utc, date_prev_ticks = read_datetime(f)
    n_streams = read_byte(f)
    print('File version =', version)
    #print('Name of the writer program =', program_name)
    #print('Comment =', comment)
    print('Date the file has been witten =', file_start_date_utc)
    #print('msc =', utc_to_msc(file_start_date_utc).strftime('%Y-%m-%d %H:%M:%S'))
    print('Number of streams in the file =', n_streams)

    if n_streams != 1:
        print(n_streams)
        raise

    if version != 4:
        print(version)
        raise

    return f, n_streams, date_prev_ticks

def read_stream_header(f):
    idd = '0x%x' % read_byte(f)
    s_id = ''
    if idd == '0x20':
        s_id = 'deals'
    if idd == '0x10':
        s_id = 'stock'
    if s_id == '':
        print(idd)
        raise
    #if 0x50 then without read_string
    return s_id, read_string(f)

def read_growing(f, prev):
    d = read_u_leb128(f)
    if d == 268435455:
        #print('here')
        res = read_leb128(f)
        #print(res)
    else:
        #print(prev,d)
        res = prev + d

    return res

def read_frame_header(f, n_streams, prev_ticks):
    #print('reading frame header')
    datetime, prev_ticks = read_growing_date_time(f, prev_ticks)
    stream_id = 0
    if n_streams > 1 and not EOF:
        stream_id = read_byte(f)
    return datetime, prev_ticks, stream_id

def bits2bool(s):
    return [c == '1' for c in s]

class QshRecord:
    def __init__(self, price_step, datetime_prev_ticks, ori_filename):
        self.byte = '00000000'
        self.trade_grow_datetime = 0
        self.datetime_prev_ticks = datetime_prev_ticks
        self.growing_trade_number = 0
        self.rel_order_number = 0
        self.rel_price = Decimal('0.00')
        self.leb_volume = 0
        self.rel_open_interest = 0
        self.direction = None
        self.price_step = price_step
        self.file = open('csv/' + ori_filename[:-3] + 'csv', 'w')
        fieldnames = ['date', 'price', 'dir',
                      'volume']
        self.csv_writer = csv.DictWriter(self.file, delimiter=',',
                                               fieldnames=fieldnames)
        self.csv_writer.writeheader()


    def read_deal_frame(self, f):
        b = read_byte(f)
        if b is None:
            return True
        frame_byte = ((bin(b)[2:]).zfill(8))[::-1]#reverse
        #print(frame_byte)
        direc = frame_byte[:2]
        if direc == '00':
            self.direction = 'nan'
        elif direc == '01':
            self.direction = 'buy'
        elif direc == '10':
                self.direction = 'sell'
        elif direc == '11':
                self.direction = 'reserved'
        else:
            print(frame_byte)
            raise
        flags = bits2bool(frame_byte)

        if flags[2]:
            #print('reading tickdatetime')
            self.trade_grow_datetime, self.datetime_prev_ticks =\
                        read_growing_date_time(f, self.datetime_prev_ticks)

        if flags[3]:
            self.growing_trade_number = read_growing(f, self.growing_trade_number)

        if flags[4]:
            self.rel_order_number += read_leb128(f)

        if flags[5]:
            self.rel_price += read_leb128(f)*self.price_step

        if flags[6]:
            self.leb_volume = read_leb128(f)

        if flags[7]:
            self.rel_open_interest += read_leb128(f)

        self.write_record()
        return False

    def write_record(self):
        self.csv_writer.writerow({\
                'date' : self.trade_grow_datetime,
                #'trade_num' : self.growing_trade_number,
                #'order_num' : self.rel_order_number,
                'price' : self.rel_price.quantize(self.price_step,
                    context=Context(traps=[Inexact])),
                'dir' : self.direction,
                'volume' : self.leb_volume,
                #'opn_int' : self.rel_open_interest,
                })

stock_list = ['GAZP', 'GMKN', 'HYDR', 'LKOH', 'MGNT', 'MTSS', 'NVTK', 'ROSN',
              'RTKM', 'SBER', 'SNGS', 'TATN', 'TRNFP', 'URKA', 'VTBR']
import os
for stock_num, stock in enumerate(stock_list):
    end = dt.datetime(2015, 3, 27)
    start = dt.datetime(2013, 4, 1)
    delta = dt.timedelta(days=1)
    d = start
    while d<=end:
        filename = "{0}/Ticks.{0}.{1}.qsh".format(stock, d.strftime('%Y-%m-%d'))
        if not os.path.exists(filename):
            d += delta
            continue
        print(stock, stock_num, '/ 15')
        f = open(filename, "rb")
        ff = f.read()
        f.close()

        f, n_streams, datetime_prev_ticks = read_file_header(ff)

        stream_type, stock_code = read_stream_header(f)
        connector, ticker, auxcode, idd, price_step = stock_code.split(':')
        #getcontext().prec = len(price_step)
        price_step = Decimal(price_step)
        #print(price_step)
        #print('msc =', utc_to_msc(frame_header_datetime).strftime('%Y-%m-%d %H:%M:%S.%f'))

        rec = QshRecord(price_step, datetime_prev_ticks, filename)
        i = 0
        frame_datetime_prev_ticks = 0#datetime_prev_ticks
        EOF = False
        while True:
            #print('Line',i)
            frame_header_datetime, frame_datetime_prev_ticks, stream_id =\
                            read_frame_header(f, n_streams, frame_datetime_prev_ticks)
            if EOF:
                break
            break_it = rec.read_deal_frame(f)
            if break_it:
                break
            i += 1

        rec.file.close()
        d += delta
    if input('Continue? y/n : ') == 'y':
        continue
    else:
        print('Completed ', stock, stock_num)
        break



