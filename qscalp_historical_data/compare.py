import csv
from decimal import Decimal
import datetime as dt
qscalp = open('/home/saya/py_invest/qscalp_data/csv/GAZP/Ticks.GAZP.2013-04-12.csv')
qscalp_reader = csv.DictReader(qscalp)
finam = open('GAZP_130412_130412.csv')
finam_reader = list(csv.DictReader(finam))
for i, row in enumerate(qscalp_reader):
    if Decimal(row['price']) == Decimal(finam_reader[i]['<LAST>']) and row['volume'] ==\
            finam_reader[i]['<VOL>'] and\
                     finam_reader[i]['<DATE>']+finam_reader[i]['<TIME>'] ==\
                     dt.datetime.strptime(row['date'], '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S'):
        print(True)
    else:
        print(i)
        raise
