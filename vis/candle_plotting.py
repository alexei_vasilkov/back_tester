from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
def make_candle(t, open, high, low, close, width=0.2, colorup='k', colordown='r',
                alpha=1.0, lh_line_width=0.7):

        OFFSET = width / 2.0

        if close >= open:
            color = colorup
            lower = open
            higher = close
            height = close - open
        else:
            color = colordown
            lower = close
            higher = open
            height = open - close

        l_line = Line2D(
            xdata=(t, t), ydata=(low, lower),
            color=color,
            linewidth=lh_line_width,
            antialiased=True,
        )

        h_line = Line2D(
            xdata=(t, t), ydata=(high, higher),
            color=color,
            linewidth=lh_line_width,
            antialiased=True,
        )

        rect = Rectangle(
            xy=(t - OFFSET, lower),
            width = width,
            height = height,
            facecolor = color,
            edgecolor = color,
            lw = 1,
        )
        rect.set_alpha(alpha)

        return l_line, h_line, rect

def candlestick_ohlc(ax, data, width=0.2, colorup='k', colordown='r',
                alpha=1.0, lh_line_width=0.7):

    OFFSET = width / 2.0

    lines = []
    patches = []
    for q in data:

        t, open, high, low, close = q[:5]

        if close >= open:
            color = colorup
            lower = open
            higher = close
            height = close - open
        else:
            color = colordown
            lower = close
            higher = open
            height = open - close

        l_line = Line2D(
            xdata=(t, t), ydata=(low, lower),
            color=color,
            linewidth=lh_line_width,
            antialiased=True,
        )

        h_line = Line2D(
            xdata=(t, t), ydata=(high, higher),
            color=color,
            linewidth=lh_line_width,
            antialiased=True,
        )

        rect = Rectangle(
            xy=(t - OFFSET, lower),
            width = width,
            height = height,
            facecolor = color,
            edgecolor = color,
            lw = 1,
        )
        rect.set_alpha(alpha)

        lines.append(l_line)
        lines.append(h_line)

        patches.append(rect)

        ax.add_line(l_line)
        ax.add_line(h_line)

        ax.add_patch(rect)
    ax.autoscale_view()

    return lines, patches

