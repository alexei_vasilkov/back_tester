import time
import datetime as dt
import numpy as np
import matplotlib
matplotlib.use('TkAgg') # <-- THIS MAKES IT FAST!
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
from back_tester.vis.my_multicursor import MultiCursor
from back_tester.vis.candle_plotting import candlestick_ohlc
from collections import deque
import math
import pylab
from urllib.request import urlopen
from matplotlib.patches import Rectangle

from matplotlib import gridspec

millnames=['','K','M','G','T']

xlim_margin = 3
def millify(n, pos):
    n = float(n)
    if n == 0:
        return '0'
    millidx=max(0,min(len(millnames)-1,
                      int(math.floor(math.log10(math.fabs(n))/3))))
    return '%.1f %s'%(n/10**(3*millidx),millnames[millidx])

from matplotlib.dates import strpdate2num

def bytespdate2num(fmt, encoding='utf-8'):
    strconverter = strpdate2num(fmt)
    def bytesconverter(b):
        s = b.decode(encoding)
        return strconverter(s)
    return bytesconverter

def xtick_formatter(x, fmt, td, p):
    res = None
    if x < p[-1][0]:
        res = (p[-1][1] + (x - p[-1][0]) * td).strftime(fmt)
    else:
        #it's reversed
        for i in p:
            if i[0] <= x:
                res = (i[1] + (x - i[0]) * td).strftime(fmt)
                break
    return res

def get_styles():
    style = dict(
        labels_fnt_size = 25,
        #colors
        color_around_plot = '#07000d',#will not be savesd in img. googlehowtosave
        color_candle_chart = '#07000d',
        color_volume_chart = '#07000d',

        color_up = '#007bac',

        color_down = '#b90000',

        color_grid = '#C9C9C9',
        color_labels = 'w',
        color_spines = '#5998ff',
        color_volume_fill = '#00ffe8',
        bar_edge_color = '#6291DE',
        candle_width = 0.94#because date[1]-date[0]==1
    )
    return style

premarket_gap = 3
def init_with_dictdf(data):
    if data['stock_df'] is not None:
        stock_df = data['stock_df']
    else:
        print('There is no stock data to plot')
        raise

    plain_date = stock_df.index.to_pydatetime()
    premarket_positions = None
    time_fmt = '%H:%M'
    date_dummy = None

    date = None
    #CHECK
    if plain_date[1].day == plain_date[0].day:
        arr = [(0, plain_date[0])]
        #print(arr[-1])
        date = [0]
        date_dummy = [0]
        c = 1
        for i in range(1, len(plain_date)):
            if plain_date[i].day != plain_date[i - 1].day:
                for j in range(premarket_gap):
                    date_dummy.append(None)
                c += premarket_gap
                arr.append((c, plain_date[i]))
            date.append(c)
            date_dummy.append(i)
            c += 1
        #print(arr[-1])
        #arr.append((c + premarket_gap, arr[-1][1] + dt.timedelta(days=1)))
        #print(arr[-1])
        premarket_positions = deque(list(reversed(arr)))
    else:
        date = list(range(len(plain_date)))
        date_dummy = list(date)

    xlim = [date[0] - xlim_margin, date[-1] + xlim_margin]

    openp = stock_df['open'].tolist()
    highp = stock_df['high'].tolist()
    lowp = stock_df['low'].tolist()
    closep = stock_df['close'].tolist()
    volume = stock_df['volume'].tolist()

    stock_name = data['stock_name']


    candle_list = list(zip(date, openp, highp, lowp, closep))

    date_len = len(plain_date)
    #if remove_gaps:
    df_dt = plain_date[1] - plain_date[0]
    #TODO
    #make choice of format based on interval
    #make lines on every day's beginning
    #
    ax1ticker_formatter = lambda x, y :\
            xtick_formatter(x, time_fmt, df_dt, premarket_positions)
    premarkets = []
    for x in reversed(premarket_positions):
        premarkets.append(make_premarket_patch(x))




    return (candle_list, date, volume, stock_name, xlim, ax1ticker_formatter,
            premarkets, premarket_positions, plain_date)
def make_premarket_patch(x):
    rect = Rectangle(
                xy=(x[0] - premarket_gap - 0.5, 15),
                width = premarket_gap,
                height = 150,
                facecolor = '#0026FF',
                edgecolor = '#0026FF',
                lw = 1,
            )

    rect.set_alpha(0.1)
    return rect

def make_volume_bar(x, volume, style):
    rect = Rectangle(
                xy=(x, 0),
                width = style['candle_width'],
                height = volume,
                facecolor = style['color_up'],
                edgecolor = style['bar_edge_color'],
                lw = 1,
            )

    rect.set_alpha(0.7)
    return rect

def set_fancy(fig, ax1, ax2, style, xlim, stock_name, ax1ticker_formatter):
    ax1.set_ylabel('Stock Price', fontsize=style['labels_fnt_size'])
    ax1.set_xlim(xlim)

    #adding a grid
    ax1.xaxis.grid(False)
    ax1.yaxis.grid(True, color=style['color_grid'], ls=u'--', linewidth=0.3)

    ax2.set_xlim(xlim)
    ax2.set_ylabel('Volume', fontsize=style['labels_fnt_size'])

    ax2.xaxis.grid(False)
    ax2.yaxis.grid(True, color=style['color_grid'], ls=u'--', linewidth=0.3)

    y_vol_formatter = matplotlib.ticker.FuncFormatter(millify)
    ax2.yaxis.set_major_locator(mticker.MaxNLocator(7, prune='upper'))
    ax2.yaxis.set_major_formatter(y_vol_formatter)
    ax2.tick_params(axis='y', which='major', labelsize=9)

    ax1ticker = matplotlib.ticker.FuncFormatter(ax1ticker_formatter)
    ax1.xaxis.set_major_formatter(ax1ticker)
    ax1.xaxis.set_major_locator(mticker.MaxNLocator(20))

    for ax in [ax1, ax2]:
        plt.setp(list(ax.spines.values()), color=style['color_spines'])
        ax.yaxis.label.set_color(style['color_labels'])
        ax.yaxis.tick_right()
    plt.setp(ax1.get_xticklabels(), visible=False)

    for label in ax2.xaxis.get_ticklabels():
        label.set_rotation(45)
    ax1.tick_params(axis='y', colors=style['color_labels'])
    for ax in [ax2]:
        ax.tick_params(axis='x', colors=style['color_labels'])
        ax.tick_params(axis='y', colors=style['color_labels'])

    #position on canvas sort of before last button on the plot
    fig.subplots_adjust(left=.05, bottom=.12, right=.95, top=.95,
                        wspace=.20, hspace=.02)

    #legend and other labeling
    #Title of the graph
    fig.suptitle(stock_name, fontsize=style['labels_fnt_size'],
                 color=style['color_labels'])
    ax2.set_xlabel('Date', fontsize=style['labels_fnt_size'])

    #show cursor as intersection



def graph_stock_base(fig, fig_canvas, data):
    #                        bytespdate2num('%Y-%m-%d %H:%M:%S')})
    style = get_styles()
    candle_list, date, volume, stock_name, xlim,\
            ax1ticker_formatter, premarket_patches, premarket_que, plain_dates = init_with_dictdf(data)
    grid_specs = gridspec.GridSpec(2, 1, height_ratios=[6, 1])
    ax1 = fig.add_subplot(grid_specs[0],\
            axisbg=style['color_candle_chart'])


    #ax1 = plt.subplot2grid((7, 4), (1, 0), rowspan=5, colspan=4,
    #                       axisbg=style['color_candle_chart'])

    candles = candlestick_ohlc(ax1, candle_list, width=style['candle_width'], colorup=style['color_up'],
                     colordown=style['color_down'], lh_line_width=1, alpha=0.85)#, shadow_line_width=0.8)


    ax2 = fig.add_subplot(grid_specs[1], sharex=ax1,
                           axisbg=style['color_volume_chart'])

    #!!!!! so that x tick was in the middle of the bar align='center'
    volume_bars = ax2.bar(date, volume, width=style['candle_width'], align='center',
            color=style['color_up'], edgecolor=style['bar_edge_color'], alpha=0.7,
            linewidth=1)
    set_fancy(fig, ax1, ax2, style, xlim, stock_name, ax1ticker_formatter)

    cursor = MultiCursor(fig_canvas, (ax1, ax2), useblit=True, color='green',
                         horizOn=True, linewidth=1)
    for rect in premarket_patches:
        ax1.add_patch(rect)

    return [ax1, ax2], (candles, volume_bars, premarket_patches, premarket_que,
            plain_dates, cursor), style

    """
    SHOWING ANNOTATIONS
    ax1.annotate('Big news!', (date[50], highp[50]), xytext=(0.8, 0.8),
                 textcoords='axes fraction',#lower left coordinates
                 arrowprops=dict(facecolor='y', shrink=0.05),
                 fontsize='10', color='w',
                 horizontalalignment='right',
                 verticalalignment='top')
    """



def plot_this_shit(data):
    color_around_plot = '#07000d'#will not be savesd in img. googlehowtosave
    fig = plt.figure(figsize=(7, 4), facecolor=color_around_plot)
    graph_stock_base(fig, fig.canvas, data)

    callback = Index()
    #x,y w,h
    axprev = plt.axes([0.7, 0.9, 0.1, 0.075])
    axnext = plt.axes([0.81, 0.9, 0.1, 0.075])
    bnext = Button(axnext, 'Next')
    bnext.on_clicked(callback.next)
    bprev = Button(axprev, 'Previous')
    bprev.on_clicked(callback.prev)

    plt.show()

from back_tester.engine.df_converter import get_df_between_dates

def di():
    data = {}
    data['stock_df'] = get_df_between_dates('/home/saya/py_invest/qscalp_data/', 'GAZP',
                   dt.datetime(2013, 4, 1), dt.datetime(2013, 4, 1), '1min')
    data['stock_name'] = 'GAZP'
    plot_this_shit(data)

from matplotlib.widgets import Button
"""
import matplotlib.finance as ff
import my_candlestick as my_c
ff._candlestick = my_c._candlestick
"""

class Index:
    ind = 0
    def next(self, event):
        self.ind += 1
        #i = self.ind % len(freqs)
        #ydata = np.sin(2*np.pi*freqs[i]*t)
        #l.set_ydata(ydata)
        plt.draw()

    def prev(self, event):
        self.ind -= 1
        #i = self.ind % len(freqs)
        #ydata = np.sin(2*np.pi*freqs[i]*t)
        #l.set_ydata(ydata)
        plt.draw()
#di()
