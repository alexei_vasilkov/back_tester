""" Module string """
import matplotlib
matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg,
                                               NavigationToolbar2TkAgg)
import threading
from matplotlib.figure import Figure
from matplotlib import style
import numpy as np
from profilehooks import profile
import tkinter as tk
from tkinter import ttk#like css
from tkinter import Frame
from tkinter import StringVar
import time

import operator
from display import graph_stock_base
from back_tester.engine import data as data_handlers
import datetime as dt
from collections import deque
from display import xlim_margin
from display import premarket_gap, make_premarket_patch, make_volume_bar
from candle_plotting import make_candle
#import tkinter.font as tkFont


LARGE_FONT = ('Verdana', 12)
NORM_FONT = ('Verdana', 10)
SMALL_FONT = ('Verdana', 8)
BOLD_FONT = ('Verdana', 10, 'Bold')

style.use('ggplot')

dat_counter = 9000
#counter for update if you clicked on menu for ex to add indicator

def from_time_to_pos(x, td, p):
    res = None
    k = 0
    if x < p[-1][1]:
        res = (x - p[-1][1])/td + p[-1][0]
    else:
        #it's reversed
        for k, i in enumerate(p):
            if i[1] <= x:
                res = (x - i[1])/td + i[0]
                break
    return int(res), len(p) - k - 1


def frame_def_config(frame):
    frame.grid_columnconfigure(0, weight=1)
    frame.grid_rowconfigure(0, weight=1)

def popupmsg(msg):
    popup = tk.Tk()
    popup.wm_title('!')
    label = ttk.Label(popup, text=msg, font=NORM_FONT)
    label.pack(side='top', fill='x', pady=10)
    b1 = ttk.Button(popup, text='Ok', command=popup.destroy)
    b1.pack()
    popup.mainloop()


class TradingGUIPlatform(tk.Tk):
    """
    Here's base class for this Trading platform.
    GUI part that is.
    """

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)#args kwargs are not mondatory
        #tk.Tk.iconbitmap(self, default='filenamehere.ico')
        tk.Tk.wm_title(self, 'Trading Platform blessed by Lain')



        container = tk.Frame(self)#window
        #for easy layout pack for difficult - grid
        #fill fill in the space you've allowed
        #expand if there is any more white space then expand beyound your limits
        container.pack(side='top', fill='both', expand=True)
        #first arg - minimum size, w - priority
        frame_def_config(container)

        menubar = tk.Menu(container)
        filemenu = tk.Menu(menubar, tearoff=0)#not makable to window
        filemenu.add_command(label='Save settings',
                             command = lambda: popupmsg('Not supported just yet'))
        filemenu.add_separator()#line between choices
        filemenu.add_command(label='Exit', command=quit)

        menubar.add_cascade(label='File', menu=filemenu)

        exchange_choice = tk.Menu(menubar, tearoff=1)

        tk.Tk.config(self, menu=menubar)


        self.frames = {}

        for F in (StartPage, ChartPage):
            #frame at the top is the frame with wich we are interacting
            frame = F(container, self)

            self.frames[F] = frame

            #sticky - north south east west alingment + stretch
            #to what alignn
            frame.grid(row=0, column=0, sticky='nsew')

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        #raise it to the front
        frame.tkraise()

def qf():
    print('You did it!')

#can't do it. use lambda
def qf1(string_to_print):
    print('You did it1!')


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        #adding text to the window
        label = tk.Label(self, text='ALPHA Bitcoin app,\nuse at your own risk',
                         font=LARGE_FONT)
        #too basic example, use grid
        label.pack(pady=10, padx=10)

        #button1 = ttk.Button(self, text='Visit Page 1',
        #                    command=lambda: qf1('See'))
        button1 = ttk.Button(self, text='Agree',
                            command=lambda: controller.show_frame(ChartPage))

        button1.pack()
        button2 = ttk.Button(self, text='Disagree',
                             command=quit)

        button2.pack()



class ChartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        #frame_def_config(self)
        self.grid_columnconfigure(0, weight=1)
        #self.configure(bg='red')
        #adding text to the window

        label = ttk.Label(self, text='Charts', font=LARGE_FONT)
        #too basic example, use grid
        label.grid(row=0, columnspan=200, sticky='N')

        self.fps_label = StringVar()
        self.fps_label.set('0.0 fps')
        fps_label = ttk.Label(self, textvariable=self.fps_label, font=LARGE_FONT)
        fps_label.grid(row=0, columnspan=200, sticky='W')

        #button1 = ttk.Button(self, text='Visit Page 1',
        #                    command=lambda: qf1('See'))
        button1 = ttk.Button(self, text='Back to home!',
                            command=lambda: controller.show_frame(StartPage))

        button1.grid(row=0, columnspan=200, sticky='E')

        next_btn = ttk.Button(self, text='Next',
                            command=lambda: self.next_candle())

        next_btn.grid(row=1, column=99, sticky='NE')

        slower_btn = ttk.Button(self, text='\u25C4\u25C4',
                            command=lambda: self.change_timer_speed(1))

        slower_btn.grid(row=1, column=100, sticky='NE')

        self.play_btn = ttk.Button(self, text='\u25B6',
                            command=lambda: self.play())

        self.play_btn.grid(row=1, column=101, sticky='NE')

        faster_btn = ttk.Button(self, text='\u25BA\u25BA',
                            command=lambda: self.change_timer_speed(-1))

        faster_btn.grid(row=1, column=102, sticky='NE')


        color_around_plot = '#07000d'#will not be savesd in img. googlehowtosave
        f = Figure(figsize=(7, 4), dpi=100, facecolor=color_around_plot)
        self.chart_fig = f
        canvas = FigureCanvasTkAgg(f, self)
        self.chart_can = canvas

        #plt.show() normally but instead
        canvas.show()
        fig_frame = canvas.get_tk_widget()
        fig_frame.grid(row=5, rowspan=100, columnspan=200, sticky='NEWS')
        self.grid_rowconfigure(5, weight=1)
        toolbar_frame = Frame(self)
        toolbar_frame.grid_columnconfigure(0, weight=1)
        toolbar_frame.grid(row=105, column=0, columnspan=200, sticky='WE')
        toolbar = NavigationToolbar2TkAgg(canvas, toolbar_frame)
        toolbar.update()


        self.timer_speed = 0.01
        self.init_chart()
        self.data_handler.set_last_unit('GAZP', '5min', len(self.candles[1])-1)
        self.chart_can.show()
        self.playing = False
        self.animation = None
        self.chart_data = []

    def init_chart(self):
        interval = '5min'
        symbol = 'GAZP'
        data_handler = data_handlers.HistoricDFDataHandler(None,\
            '/home/saya/py_invest/qscalp_data/', [symbol], dt.datetime(2013, 4,
                22), interval=interval)
        self.data_handler = data_handler
        #interval = '5min'
        to_plot = {}
        to_plot['stock_df'] = data_handler.get_last_data()[symbol][interval]
        to_plot['stock_name'] = symbol

        color_around_plot = '#07000d'#will not be savesd in img. googlehowtosave
        self.ax, p, self.style = graph_stock_base(self.chart_fig,
                                                            self.chart_can, to_plot)
        candles, volume_bars, premarket_patches, premarket_que, plain_dates,\
                                        self.cursor = p

        self.candles = (deque(candles[0]), deque(candles[1]))
        self.premarket_patches = deque(premarket_patches)
        self.premarket_que = premarket_que
        self.plain_dates = deque(plain_dates)
        self.volume_bars = deque(tuple(volume_bars))
        self.last_frame_update = time.time()
        self.ax0_y_margin = 0.25
        self.ax0_y_window = self.ax[0].get_ylim()
        x = self.ax[0].get_xlim()
        self.ax[0].set_xlim(x[0], x[1] + 2)
        self.ax0_y_window = self.ax0_y_window[1] - self.ax0_y_window[0]
        self.td = self.plain_dates[1] - self.plain_dates[0]
        self.busy = False
        print('plotted')
        #line pairs, rectangles, rects, rects
        #for x in self.plain_dates:
        #    from_time_to_pos(x, self.td, self.premarket_que)
        self.signals_que = deque()
        for i in self.plain_dates:
            p = from_time_to_pos(i, self.td, self.premarket_que)
            #print(p)
            #print(p[0] - p[1]*premarket_gap -
            #        self.candles[0][0].get_xdata()[0])
            #print(self.candles[0][0].get_xdata()[0])
            candle_number = p[0] - p[1]*premarket_gap -\
                            self.candles[0][0].get_xdata()[0]
            self.signals_que.append(self.plot_signal('buy', p[0],
                                                     candle_number))
            self.signals_que.append(self.plot_signal('sell', p[0],
                                                     candle_number))


    def plot_signal(self, direc, x_pos, candle_number):
        if direc == 'buy':
            color = 'green'
            y = self.candles[0][candle_number*2].get_ydata()[0]
            op = operator.sub
        else:
            color = 'red'
            y = self.candles[0][candle_number*2 + 1].get_ydata()[0]
            op = operator.add

        y1 = op(0, -0.15)
        y2 = op(y, 0.18)

        #print(x_pos, y2, y1)
        return (self.ax[0].arrow(x_pos, y2, 0, y1, width=0.5, head_width=0.8,
                                head_length=0.02, fc=color, ec=color), x_pos)
        #return self.ax[0].annotate(text, xy=(x_pos, y1), xytext=(x_pos - 0.5, y2),
        #                    color=color,
        #                    arrowprops=dict(facecolor=color, headwidth=4,
        #                                    width=2, edgecolor=color, shrink=0.05),
        #                   )

    def change_timer_speed(self, direc):
        self.timer_speed += 0.3*direc
        if self.timer_speed < 0.01:
            self.timer_speed = 0.01

    def pause(self):
        self.playing = False
        self.cursor.set_visible(True)
        self.fps_label.set('0.0 fps')


    def next_candle(self):
        if self.busy or self.playing:
            #just disable this btn in the future
            return
        self.show_next_frame()


    def show_next_frame(self):
        x1 = self.ax[0].get_xlim()
        q = x1[1] + 1
        self.ax[0].set_xlim([x1[0] + 1, x1[1] + 1])
        left_most = int(x1[0] + xlim_margin)
        if self.premarket_que[-1][0] < left_most:#x1[0]:
            #print(self.premarket_que[-1], x1)
            print('one premarket removed')
            self.premarket_que.pop()
            self.premarket_patches.popleft().remove()

        first_premarket_part = self.premarket_que[-1][0] - left_most

        new_bar = False
        if self.candles[0][0].get_xdata()[0] < left_most:
            #pops two candle lines
            self.candles[0].popleft().remove()
            self.candles[0].popleft().remove()
            #pops candle rect
            self.candles[1].popleft().remove()

            self.plain_dates.popleft()

            v = self.volume_bars.popleft()
        else:
            v = make_volume_bar(0, 0, self.style)
            new_bar = True

        if q - xlim_margin <= self.premarket_que[0][0]:
            #compute and display fps
            b = time.time()
            fps = '%0.2f fps' % (1/(b-self.last_frame_update))
            self.fps_label.set(fps)
            self.last_frame_update = b
            #update chart
            self.chart_can.draw()
            return


        #gets next candle data
        c = self.data_handler.get_next_unit('GAZP', '5min')
        self.plain_dates.append(c.name.to_pydatetime())
        #print(c)
        #change limit if new candle beyond current limit
        ylim = self.ax[0].get_ylim()
        changed = False

        if c[2] <= ylim[0]:
            changed = True
            y0 = c[2] - self.ax0_y_margin
            ylim = (y0, y0 + self.ax0_y_window)

        elif c[1] >= ylim[1]:
            changed = True
            y1 = c[1] + self.ax0_y_margin
            ylim = (y1 - self.ax0_y_window, y1)

        if changed:
            self.ax[0].set_ylim(ylim)
        #compute next x coordinate
        next_x = self.candles[0][-1].get_xdata()[0] + 1
        #print(self.premarket_que)
        #print(len(self.premarket_patches))
        #print(self.premarket_que)
        #print(len(self.premarket_patches))
        #check for gap between candles and fil it with premarket rect
        #print(self.premarket_que)
        if self.plain_dates[-2].day != self.plain_dates[-1].day:
            print('new premarket')
            #TODO see screenshot
            #candles are slowpokes etc
            next_x += premarket_gap
            self.premarket_que.appendleft((next_x, self.plain_dates[-1]))
            self.premarket_patches.append(make_premarket_patch(self.premarket_que[0]))
            self.ax[0].add_patch(self.premarket_patches[-1])
            #self.ax[0].autoscale(False)
            #self.ax[0].plot([self.premarket_que[-1][0] - premarket_gap - 0.5]*2, [118, 124])
            #self.ax[0].plot([self.premarket_que[-1][0]-0.5]*2, [118, 124])
            #self.ax[0].plot([self.premarket_que[1][0] - premarket_gap - 0.5]*2, [118, 124])
            #self.ax[0].plot([self.premarket_que[1][0]-0.5]*2, [118, 124])

        left_candle = left_most
        if self.signals_que and self.signals_que[0][1] < left_candle:
            for i in range(10 if len(self.signals_que) >= 10 else
                           len(self.signals_que)):
                #print(self.signals_que[i][1], left_candle)
                if self.signals_que[0][1] < left_candle:
                    self.signals_que.popleft()[0].remove()
        #take left rect from bar plot and move it to the right
        #so it can be volume for a new candle
        v.set_x(next_x)
        v.set_height(c[-1])
        if new_bar:
            self.ax[1].add_patch(v)
        self.volume_bars.append(v)
        #get candle lines and rectangle
        c = make_candle(next_x, c[0], c[1], c[2], c[3],
                width=self.style['candle_width'], colorup=self.style['color_up'],
                     colordown=self.style['color_down'], lh_line_width=1,
                     alpha=0.85)
        #add lines and rectangle to deques
        self.candles[0].extend(c[:2])
        self.candles[1].append(c[2])
        #add candle to a plot
        self.ax[0].add_line(c[0])
        self.ax[0].add_line(c[1])
        self.ax[0].add_patch(c[2])



        p = from_time_to_pos(self.plain_dates[-1], self.td, self.premarket_que)
        print(left_most, first_premarket_part)
        if first_premarket_part > premarket_gap:
            first_premarket_part = premarket_gap
        print(first_premarket_part)
        #self.ax[0].autoscale(False)
        #self.ax[0].plot([left_most, left_most], [118, 124])
        candle_number = p[0] - p[1]*premarket_gap -\
                        left_most - first_premarket_part
        print(candle_number)
        if candle_number + 1 != len(self.candles[1]):
            print('-'*40)
        self.signals_que.append(self.plot_signal('buy', p[0],
                                                 candle_number))
        self.signals_que.append(self.plot_signal('sell', p[0],
                                                 candle_number))




        #compute and display fps
        b = time.time()
        fps = '%0.2f fps' % (1/(b-self.last_frame_update))
        self.fps_label.set(fps)
        self.last_frame_update = b
        #update chart
        self.chart_can.draw()


    #profile
    def play_cycle(self):
        if self.busy or not self.playing:
            return

        self.busy = True

        self.show_next_frame()

        self.busy = False
        #schedule next update
        threading.Timer(self.timer_speed, self.play_cycle).start()

    def play(self):
        global anim, figa
        if self.play_btn['text'] == '||':
            self.pause()
            self.play_btn['text'] = '\u25B6'
            return

        self.cursor.set_visible(False)
        self.play_btn['text'] = '||'
        self.busy = False
        self.playing = True
        threading.Thread(target=self.play_cycle).start()

from matplotlib import pyplot as plt

#f = Figure()
#a = f.add_subplot(111)#numbers subplots
app = TradingGUIPlatform()
#f = Figure(figsize=(6, 4), dpi=100)
app.geometry('1280x720')
app.mainloop()
